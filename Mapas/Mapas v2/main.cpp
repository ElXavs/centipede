#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Pulga.h"
#include "Hongo.h"
#include "Arania.h"
#include "Jugador.h"
#include "Disparo.h"
#include "Escorpion.h"
#include "Centipede.h"
#include "Sonidos.h"

#define Arriba 72
#define Abajo 80
#define Izquierda 75
#define Derecha 77
#define Espacio 32

using namespace std;

Centipede centipede[12];
Hongo hongos[170];
Sonidos sounds;

void colores(int X)
{
SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),X);
}

void TamanioConsola(int x, int y)
{
	SMALL_RECT rect;
	COORD coord;
	coord.X = x;
	coord.Y = y;
	rect.Top = 0;
	rect.Left = 0;

	rect.Bottom = coord.Y - 1;
	rect.Right = coord.X - 1;

	HANDLE hwnd = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleScreenBufferSize(hwnd, coord);
	SetConsoleWindowInfo(hwnd, TRUE, &rect);
}

void gotoxy(int x, int y)
{
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon, dwPos);
}

void SinCursor()
{
    HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = 20;
	cci.bVisible = FALSE;

	SetConsoleCursorInfo(hCon, &cci);

}

void victoria (int puntaje)
{
    sounds.snd_win(3);
    system("cls");
    SinCursor();
    TamanioConsola(32,38);
    colores(96);
    for (int x=0; x<32; x++){
        for (int y=0; y<40; y++){
            if (y%2==0){
                gotoxy(x,y);
                cout<<" ";
            }
            else{
                gotoxy(31-x,y);
                cout<<" ";
            }
        }
        Sleep(50);
    }
    colores(103);
    gotoxy(0,5);
    cout<<"      8o  o8  o88o  8    8"<<endl;
    gotoxy(0,6);
    cout<<"       8oo8  88  88 8    8"<<endl;
    gotoxy(0,7);
    cout<<"        88   88  88 8o  o8"<<endl;
    gotoxy(0,8);
    cout<<"        88    8888   8888  "<<endl;

    cout<<endl;
    gotoxy(3,10);
    cout<<"8o        o8 888 8o  8    8"<<endl;
    gotoxy(3,11);
    cout<<" 8o  oo  o8   8  88oo8    8"<<endl;
    gotoxy(3,12);
    cout<<"  8oo88oo8    8  8   8     "<<endl;
    gotoxy(3,13);
    cout<<"   88  88    888 8   8    O"<<endl;
    gotoxy(13,15);
    cout<<"Puntaje";
    gotoxy(14,16);
    cout<<puntaje;
    Sleep(5000);
    colores(7);
    sounds.snd_win(0);
}

void muerte (int puntaje)
{
    system("cls");
    SinCursor();
    TamanioConsola(33,38);
    sounds.snd_centipedesteps(3);
    colores(64);
    for (int y=0; y<40; y++){
        for (int x=0; x<33; x++){
            if (x%2==0){
                gotoxy(x,y);
                cout<<" ";
            }
            gotoxy(x,y-1);
            cout<<" ";
            if (y==39){
                gotoxy(x,y);
                cout<<" ";
            }
        }
        Sleep(100);
    }
    colores(71);

    gotoxy(0,5);
    cout<<"      8o  o8  o88o  8    8"<<endl;
    gotoxy(0,6);
    cout<<"       8oo8  88  88 8    8"<<endl;
    gotoxy(0,7);
    cout<<"        88   88  88 8o  o8"<<endl;
    gotoxy(0,8);
    cout<<"        88    8888   8888  "<<endl;

    cout<<endl;
    gotoxy(3,10);
    cout<<" 8    .o88o. .o88o. 8888  8"<<endl;
    gotoxy(3,11);
    cout<<" 8    88  88 88ooo. 8ooo  8"<<endl;
    gotoxy(3,12);
    cout<<" 8    8o  o8     88 8      "<<endl;
    gotoxy(3,13);
    cout<<" 8888 '8888' '8888' 8888  O"<<endl;
    gotoxy(13,15);
    cout<<"Puntaje";
    gotoxy(14,16);
    cout<<puntaje;
    Sleep(5000);
    colores(7);
    sounds.snd_centipedesteps(0);
}

//Imprime el marco
void marco()
{
    int x,y;
    int largo_mapa = 34;
    int ancho_mapa= 32;
    srand (time(NULL));
    int n=rand() % 14+1;
    colores(n+1);

    for (x=0; x<largo_mapa; x++)
    {
        for (y=0; y<ancho_mapa; y++)
        {
            if (x==1)
            {
                if (y==0)
                {
                    gotoxy(0,1);
                    printf("%c", 218);
                }
                else
                {
                    if (y>0 and y<ancho_mapa)
                    {
                        printf("%c", 196);
                    }
                    if (y==31)
                    {
                        printf("%c", 191);
                    }
                }
            }
            else {
                if (x==33)
                {
                    if (y==0)
                    {
                        printf("%c", 192);
                    }
                    else
                    {
                        if (y>0 and y<ancho_mapa)
                        {
                            printf("%c", 196);
                        }
                        if (y==31)
                        {
                            printf("%c", 217);
                        }
                    }
                }
                else
                {
                    if(x != 0)
                    {
                        if (y==0 or y==31)
                        {
                            printf("%c", 179);
                        }
                        if (y != 0 or y!=31)
                        {
                            printf("%c", 0);
                        }
                    }

                }
            }
        }
        cout<<endl;
    }
}

//Aqui se almacenan e imprimen los hongos
Hongo printhongos(Hongo hongos[170])
{
    srand (time(NULL));
    int n=rand() % 15+1;
    colores(n);
    int posx, posy;
    int generador_hongos, cont_hongos = 0;
    int suficientes;
    n = 0;
    for (posx=1; posx<32; posx++)
    {
        suficientes = 0;
        for (posy=3; posy<30; posy++)
        {
            if(posx<8)
            {
                n = 2;
                generador_hongos = rand() % 9;
            }
            if(posx >29)
            {
                n = 7;
                generador_hongos = rand() % 5;
            }
            else
            {
                n = 3;
                generador_hongos = rand() % 9;
            }
            if (generador_hongos==4 and cont_hongos <70 and suficientes<n)
            {
                hongos[cont_hongos]=Hongo(posx, posy, 100, 4, 0);
                hongos[cont_hongos].printhongo();
                cont_hongos ++;
                suficientes++;
            }
        }
    }

    for (int i = cont_hongos; i < 170; i++)
    {
        hongos[i] = Hongo(0, 0, 100, 5, 0);
    }
    return(hongos[170]);
}

//Imprime el tablero de vidas y puntuaciones
void tablero(int vidas, int puntaje, int mejorpuntaje, int nivel)
{
    gotoxy(0,0);
    cout<<"                          ";
    gotoxy(1,0);
    colores(3);
    cout<<puntaje;
    gotoxy(6,0);
    for(int x = 0; x < vidas; x++)
    {
        colores(7);
        printf("%c", 190);
    }
    gotoxy(14,0);
    colores(6);
    cout<<mejorpuntaje;
    gotoxy(24,0);
    cout<<"LEVEL "<<nivel;

}

void puntos(int puntaje)
{

    gotoxy(1,0);
    colores(3);
    cout<<puntaje;
}

//Imprime las letras del titulo
void letrascentipede()
{
    colores(2);
    gotoxy(3,5);
    cout<<" o88b 8888 8b  8 88888 888"<<endl;
    gotoxy(3,6);
    cout<<"88    8ooo 88bo8   8    8 "<<endl;
    gotoxy(3,7);
    cout<<"8b    8    8  88   8    8 "<<endl;
    gotoxy(3,8);
    cout<<" 888P 8888 8   8   8   888"<<endl;
    gotoxy(6,9);
    cout<<"888o  8888 888b  8888"<<endl;
    gotoxy(6,10);
    cout<<"8   8 8ooo 8   8 8ooo"<<endl;
    gotoxy(6,11);
    cout<<"8ooP  8    8   8 8   "<<endl;
    gotoxy(6,12);
    cout<<"8     8888 888P  8888"<<endl;
}

//Funcion que imprime en pantalla la ara�a
void printarana()
{
    SinCursor();
    colores(8);
    printf("%c", 201);
    printf("%c", 153);
    printf("%c", 187);
}

void printcentipede()
{
    SinCursor();
    colores(2);
    printf("%c",02);
    printf("%c",207);
    printf("%c",207);
    printf("%c",207);
    printf("%c",207);
    printf("%c",207);
    printf("%c",207);
    printf("%c",207);
}

void printpulga()
{
    SinCursor();
    colores(6);
    printf("%c",42);
}

void printescorpion()
{
    SinCursor();
    colores(3);
    printf("%c",245);
}

//Funcion que hace la animacion de la intro
void titulo()
{

    while (!kbhit()){
        int i, x, y, z;
        for (i=1; i<=8; i++)
        {
            colores(2);
            letrascentipede();
            switch (i)
            {
                case 1:
                    z=1;
                    for (x=1; x<=4; x++)
                    {
                        z++;
                        y = 19-z;
                        gotoxy(x, y);
                        printarana();
                        gotoxy(x, y);
                        Sleep(100);
                        cout<<"   ";
                    }
                    break;
                case 2:
                    z=-5;
                    for (x=5; x<=8; x++)
                    {
                        z++;
                        y = 19 +z;
                        gotoxy(x,y);
                        printarana();
                        gotoxy(x,y);
                        Sleep(100);
                        cout<<"   ";
                    }
                    break;
                case 3:
                    z=1;
                    for (x=9; x<=12; x++)
                    {
                        z++;
                        y = 19-z;
                        gotoxy(x, y);
                        printarana();
                        gotoxy(x, y);
                        Sleep(100);
                        cout<<"   ";
                    }
                    break;
                case 4:
                    z=-5;
                    for (x=13; x<=16; x++)
                    {
                        z++;
                        y = 19 +z;
                        gotoxy(x,y);
                        printarana();
                        gotoxy(x,y);
                        Sleep(100);
                        cout<<"   ";
                    }
                    break;
                case 5:
                    z = 1;
                    for (x=17; x<=20; x++)
                    {
                        z++;
                        y = 19-z;
                        gotoxy(x, y);
                        printarana();
                        gotoxy(x, y);
                        Sleep(100);
                        cout<<"   ";
                    }
                    break;
                case 6:
                    z=-5;
                    for (x=21; x<=24; x++)
                    {
                        z++;
                        y = 19 +z;
                        gotoxy(x,y);
                        printarana();
                        gotoxy(x,y);
                        Sleep(100);
                        cout<<"   ";
                    }
                    break;
                case 7:
                    z=1;
                    for (x=25; x<=28; x++)
                    {
                        z++;
                        y = 19-z;
                        gotoxy(x, y);
                        printarana();
                        gotoxy(x, y);
                        Sleep(100);
                        cout<<"   ";
                    }
                    break;
                case 8:
                    z=-5;
                    for (x=29; x<=30; x++)
                    {
                        z++;
                        y = 19 +z;
                        gotoxy(x,y);
                        if (x==30)
                        {
                            printf("%c", 201);
                            printf("%c", 153);
                            gotoxy(x,y);
                            Sleep(100);
                            cout<<"  ";
                        }
                        else
                        {
                            printarana();
                            gotoxy(x,y);
                            Sleep(100);
                            cout<<"   ";
                        }
                    }
                    break;

            }
        }
    }
}

//Imprime las funciones del menu
int menu()
{
    bool menu = true;
    int opc = 1;
    int x,y = 19;

    marco();
    letrascentipede();
    gotoxy(11, 19);
    colores(3);
    cout << "Iniciar Juego";
    gotoxy(11, 21);
    cout << "Instrucciones";
    gotoxy(14,23);
    cout << "Creditos";
    gotoxy(15, 25);
    cout << "Salir";
    gotoxy(10, 28);
    colores(7);
    cout << "Pulse una tecla ";

    //titulo();

    while (menu == true)
    {
        char tecla = getch();

        //Estos ifs suben o bajan el valor de la opcion desde 1 hasta 4
        if(tecla == Arriba)
        {
            if(opc > 1)
            {
                y -= 2;
                opc--;
            }
        }

        else if(tecla == Abajo)
        {
            if(opc < 4)
            {
                opc++;
                y += 2;
            }
        }

        //Dependiendo del valor de lo opcion cambia la posicion de la flecha
        if(opc == 3) x = 10;

        else if(opc == 4) x = 11;

        else x = 7;

        if(tecla == Espacio)
        {
            system("cls");
            return opc;
        }

        while (!kbhit())
        {
            gotoxy(x, y);
            cout << " -> ";
            if(kbhit()){break;}
            Sleep(200);
            gotoxy(x-1, y);
            cout << " -> ";
            if(kbhit()){break;}
            Sleep(200);
        }

            gotoxy(9, 28);
            cout << "  Pulse espacio  ";

            gotoxy(x, y);
            cout << "    ";
            gotoxy(x-1, y);
            cout << "    ";
    }
}

void creditos_instrucciones(int opc)
{
    system("cls");
    int pag = 1;

    if(opc == 2)
    {
        if(pag == 1)
        {
            marco();
            gotoxy(7,2);
            cout << " --- MOVIMIENTO ---";
            colores(3);
            gotoxy(4, 5);
            colores(3);
            printf("\x18");
            colores(7);
            cout << "    Usa las flechas";
            gotoxy(2, 6);
            colores(3);
            printf("\x1b \x19 \x1a");
            colores(7);
            cout << "  direccionales para";
            gotoxy(6,7);
            cout<<"   moverte";

            gotoxy(2, 9);
            colores(3);
            cout << " ___";
            colores(7);
            cout << " Usa la barra espaciadora";
            gotoxy(6,10);
            cout<< " para disparar";

            gotoxy(2, 25);
            cout << "Presiona la barra espaciadora ";
            gotoxy(10,26);
            cout<<"para continuar..";
            while (pag == 1)
            {
                char tecla = getch();

                if(tecla == Espacio) {
                        sounds.snd_select(2);
                        pag = 2;
                }
            }
        }

        if(pag == 2)
        {
            system("cls");
            marco();
            gotoxy(8,2);
            cout << " --- ENEMIGOS ---";
            //Centipede
            gotoxy(2,4);
            colores(3);
            cout <<"CENTIPEDE";
            gotoxy(22,4);
            printcentipede();
            gotoxy(2,6);
            colores(7);
            cout <<"El enemigo principal es un";
            gotoxy(2,7);
            cout<<"cienpies que puede dividirse";
            gotoxy(2,8);
            cout<<"en varios segmentos. Al";
            gotoxy(2,9);
            cout<<"chocar con un hongo o la pared";
            gotoxy(2,10);
            cout<<"cambia de direccion. Para";
            gotoxy(2,11);
            cout<<"derrotarlo tienes que destruir";
            gotoxy(2,12);
            cout<<"todos sus segmentos.";

            //Ara�a
            gotoxy(2,16);
            colores(3);
            cout <<"ARA";
            printf("%c", 164);
            cout<<"A";
            gotoxy(25,16);
            printarana();
            colores(7);
            gotoxy(2,18);
            cout <<"Estos enemigos son dificiles";
            gotoxy(2,19);
            cout<<"debido a su velocidad y sus";
            gotoxy(2,20);
            cout<<"patrones erraticos. Para";
            gotoxy(2,21);
            cout<<"derrotarlo basta con un";
            gotoxy(2,22);
            cout<<"disparo.";

            //Pulga
            gotoxy(2,26);
            colores(3);
            cout<<"PULGA";
            gotoxy(26,26);
            printpulga();
            colores(7);
            gotoxy(2,28);
            cout<<"Cae desde la parte superior";
            gotoxy(2,29);
            cout<<"de la pantalla en linea recta";
            gotoxy(2, 30);
            cout<<"creando hongos. Basta un";
            gotoxy(2, 31);
            cout<<"disparo.";


            while (pag == 2)
            {
                char tecla = getch();

                if(tecla == Espacio){
                        sounds.snd_select(2);
                        pag = 3;
                }
            }
        }

        if(pag == 3)
        {
            system("cls");
            marco();
            gotoxy(8,2);
            cout << " --- ENEMIGOS ---";
            //Escorpion
            gotoxy(2,4);
            colores(3);
            cout <<"ESCORPION";
            gotoxy(27,4);
            printescorpion();
            gotoxy(2,6);
            colores(7);
            cout <<"Este enemigo aparece rara";
            gotoxy(2,7);
            cout<<"vez. Tiene la capacidad de";
            gotoxy(2,8);
            cout<<"envenenar a los hongos que";
            gotoxy(2,9);
            cout<<"toca. Cuando el Centipede se";
            gotoxy(2,10);
            cout<<"topa con uno de estos hongos";
            gotoxy(2,11);
            cout<<"baja directamente a la zona";
            gotoxy(2,12);
            cout<<"del jugador. ";

            //Hongos
            gotoxy(2,17);
            colores(3);
            cout<<"HONGOS";
            gotoxy(27,17);
            colores(7);
            printf("%c", 127);
            gotoxy(2,19);
            colores(7);
            cout<<"Se encuentran esparcidos por";
            gotoxy(2,20);
            cout<<"toda la pantalla. No pueden ";
            gotoxy(2,21);
            cout<<"moverse. Para ser destruidos ";
            gotoxy(2,22);
            cout<<"se ocupan cuatro disparos.";

            while (pag == 3)
            {
                char tecla = getch();
{
                        sounds.snd_select(2);
                        pag = 4;
                }
            }
        }

        if(pag == 4)
        {
            system("cls");
            marco();
            gotoxy(7,2);
            cout << " --- COMO GANAR ---";
            gotoxy(2,6);
            colores(7);
            cout<<"Para ganar se";
            gotoxy(2,7);
            cout<<"necesita llegar";
            gotoxy(2,8);
            cout<<"al NIVEL 3 y ";
            gotoxy(2,9);
            cout<<"derrotar al CENTIPEDE de";
            gotoxy(2,10);
            cout<<"ese nivel destrullendo";
            gotoxy(2,11);
            cout<<"todas sus partes.";
            gotoxy(2,12);
            cout<<"Recuerda que para";
            gotoxy(2,13);
            cout<<"lograr esto solo";
            gotoxy(2,14);
            cout<<"cuentas con 3 vidas";
            gotoxy(2,15);
            printcentipede();

            while (pag == 4)
            {
                char tecla = getch();

                if(tecla == Espacio){
                        sounds.snd_select(2);
                        pag = 5;
                }
            }
        }
    }

    if(opc == 3)
        {
            system("cls");
            marco();
            gotoxy(7,2);
            cout << "  --- CREDITOS ---";
            gotoxy(5,6);
            colores(3);
            cout<<"UABCS IDS  2do Semestre";
            gotoxy(8,8);
            cout<<"Programacion I TM";
            gotoxy(8,10);
            cout<<"Alvarado Palacios";
            gotoxy(10,11);
            cout<<"Demian Alexis";
            gotoxy(10,13);
            cout<<"Gonzalez Vega";
            gotoxy(10,14);
            cout<<"Jose  Eduardo";
            gotoxy(8,16);
            cout<<"Sepulveda  Ibarra";
            gotoxy(10,17);
            cout<<"Cesar  Daniel";
            gotoxy(5,23);
            cout<<"Departamento Academico";
            gotoxy(3,24);
            cout<<"de Sistemas Computacionales";
            getch();
            sounds.snd_select(2);
        }
}

int ValidarVidas(int xyjugador[2])
{
    int validacion = 0;
    if (xyjugador[0] == -1)
    {
        validacion = 1;
    }
    return(validacion);
}

void CreacionCentipede()
{
    centipede[0] = Centipede(true, 20, 0);
    centipede[1] = Centipede(false, 19, 1);
    centipede[2] = Centipede(false, 18, 2);
    centipede[3] = Centipede(false, 17, 3);
    centipede[4] = Centipede(false, 16, 4);
    centipede[5] = Centipede(false, 15, 5);
    centipede[6] = Centipede(false, 14, 6);
    centipede[7] = Centipede(false, 13, 7);
    centipede[8] = Centipede(false, 12, 8);
    centipede[9] = Centipede(false, 11, 9);
    centipede[10] = Centipede(false, 10, 10);
    centipede[11] = Centipede(false, 9, 11);
}

void MovimientoCentipede(Hongo hongos[170])
{
    centipede[0].movimiento(centipede[0], hongos, centipede);
    centipede[1].movimiento(centipede[0], hongos, centipede);
    centipede[2].movimiento(centipede[1], hongos, centipede);
    centipede[3].movimiento(centipede[2], hongos, centipede);
    centipede[4].movimiento(centipede[3], hongos, centipede);
    centipede[5].movimiento(centipede[4], hongos, centipede);
    centipede[6].movimiento(centipede[5], hongos, centipede);
    centipede[7].movimiento(centipede[6], hongos, centipede);
    centipede[8].movimiento(centipede[7], hongos, centipede);
    centipede[9].movimiento(centipede[8], hongos, centipede);
    centipede[10].movimiento(centipede[9], hongos, centipede);
    centipede[11].movimiento(centipede[10], hongos, centipede);
}

int CentipedesMuertos()
{
    int muertes = 0;
    for (int i = 0; i < 12; i++)
    {
        if (centipede[i].getVida() == 0 and centipede[i].getx()>0)
        {
            muertes++;
        }
    }
    return(muertes);
}

int HongosDaniados(Hongo hongos[170], Jugador jugador, int puntaje)
{
    int i;
    if (jugador.getvidas()<3)
    {
        for (i = 0; i<170; i++)
        {
            if (i == 0){
                sounds.snd_hongos(1);
            }
            if (hongos[i].getestado() <4  && hongos[i].getestado()!=0 && hongos[i].getveneno()==0){
                puntaje=puntaje+5;
                Sleep(100);
            }
            if (hongos[i].getveneno() ==1){
                puntaje=puntaje+5;
                hongos[i].setveneno(0);
                Sleep(100);
            }
            if (hongos[i].getestado()!=0){
                    if (hongos[i].getestado() != 5){
                        hongos[i].setestado(4);
                    }
            }
            if (hongos[i].getx() != 0 and hongos[i].gety() != 0){
                hongos[i].printhongo();
            }

            if(hongos[i].getestado() == 5)
            {
                break;
            }
        }
        return puntaje;
    }
}

bool CabezasCentipede()
{
    for (int i=0; i<12; i++)
    {
        if(centipede[i].gety() == 32)
        {
            return true;
        }
    }
    return false;
}

int main()
{
    SinCursor();
    TamanioConsola(35,38);
    srand (time(NULL));
    int mejorpuntaje=1000;
    int on=1;


    while(on==1)
    {
    int opc_menu;

    Jugador jugador = Jugador();

    Disparo disparo = Disparo(hongos);

    Pulga pulguita=Pulga();
    pulguita.setpuntos(200);
    int pulgax = 2 + rand() % 20;
    int pulgastart = (1 + (rand() % 10 ))* 100;
    pulguita.Start(pulgastart, pulgax);

    Arania arana = Arania();
    int aranax = 20 + rand() % 5;
    int aranastart = (5 + (rand() % 10 )) * 100;
    arana.Start(aranastart, aranax);

    Escorpion scorpion = Escorpion();
    scorpion.setpuntos(1000);
    int escorpionx = 5 + rand() % 20;
    int escorpionstart = (5 + (rand() % 10 )) * 100;
    scorpion.Start(escorpionstart, escorpionx);

    int contadorcentipedes=0;

    int estadopul=0;
    int estadocent=0;
    int estadoscorp=0;
    int estadoara=0;
    int n = 0;
    int x = 16;
    int y = 32;
    int i = 0;
    int nivel = 1;
    int velocidad = 10;
    int xyjugador[2] = {0,0};
    int colision = 0;
    int cooldown= 0;
    int haydisparo = 0;
    int puntaje=0;

    bool cabezas = false;
    int yRandCabeza;
    int j = 20;



        sounds.snd_menu(3);
        system("cls");
        opc_menu = menu();

        switch (opc_menu)
        {
            case 1:
                SinCursor();
                sounds.snd_select(1);
                colores(3);
                marco();
                CreacionCentipede();
                cabezas = false;
                hongos[170] = printhongos(hongos);

                for (int contador=0; contador<170; contador++){
                    hongos[contador].setpuntos(0);
                }

                while (jugador.getvidas()> 0)
                {

                    n = 0;
                    marco();

                    for (int i = 0; i < 170; i++)
                    {
                        hongos[i].printhongo();

                        if(hongos[i].getestado() == 5) break;
                    }

                    if (CentipedesMuertos() == 12)
                    {
                        sounds.snd_level(2);
                        nivel++;
                        contadorcentipedes = 0;
                        if (nivel == 2){
                            velocidad = 4;
                        }
                        if (nivel == 3){
                            velocidad = 1;
                        }
                        if (nivel == 4)
                        {
                            break;
                        }
                    }
                    else
                    {
                        puntaje=HongosDaniados(hongos, jugador, puntaje);
                    }

                    if (puntaje>mejorpuntaje){mejorpuntaje=puntaje;}
                    CreacionCentipede();
                    cabezas = false;
                    tablero(jugador.getvidas(), puntaje, mejorpuntaje, nivel);
                    jugador.setx(16);
                    jugador.sety(32);

                    gotoxy(0,34);
                    cout<<"          ";

                    while(n<100000)
                    {
                        if(estadoara!=arana.PrintArania(1, n, hongos, colision)){
                            estadoara=arana.PrintArania(1, n, hongos, colision);
                            sounds.snd_coordinar(arana.PrintArania(1, n, hongos, colision),1,
                            scorpion.soundescorpion(1, n, hongos, colision),pulguita.printpulga(2, n, hongos, colision),3);
                        }
                        else if(estadopul !=pulguita.printpulga(2, n, hongos, colision)){
                            estadopul=pulguita.printpulga(2, n, hongos, colision);
                            sounds.snd_coordinar(arana.PrintArania(1, n, hongos, colision),1,
                            scorpion.soundescorpion(1, n, hongos, colision),pulguita.printpulga(2, n, hongos, colision),1);

                            switch(velocidad){
                            case 1:
                                cooldown=1000;
                                break;
                            case 4:
                                cooldown=250;
                                break;
                            case 10:
                                cooldown=100;
                                break;
                            }
                        }
                        else if(estadoscorp !=scorpion.soundescorpion(1, n, hongos, colision)){
                            estadoscorp=scorpion.soundescorpion(1, n, hongos, colision);
                            sounds.snd_coordinar(arana.PrintArania(1, n, hongos, colision),1,
                            scorpion.soundescorpion(1, n, hongos, colision),pulguita.printpulga(2, n, hongos, colision),1);

                            switch(velocidad){
                            case 1:
                                cooldown=1000;
                                break;
                            case 4:
                                cooldown=250;
                                break;
                            case 10:
                                cooldown=100;
                                break;
                            }
                        }
                        else if (cooldown<=0){
                            sounds.snd_coordinar(estadoara,1,0,0,3);

                            switch(velocidad){
                            case 1:
                                cooldown=1000;
                                break;
                            case 4:
                                cooldown=250;
                                break;
                            case 10:
                                cooldown=100;
                                break;
                            }

                        }

                        colores(2);
                        gotoxy(0,34);
                        //cout<<"Time:"<<n<<endl; //Esta linea va comentada completa, pero se puede dejar para trabajar en el codigo

                        if (n % 10 == 0)
                        {
                            MovimientoCentipede(hongos);

                            if(cabezas == false && CabezasCentipede() == true) cabezas = true;

                            if (cabezas == true)
                            {
                                if(n%500 == 0)
                                {
                                    for (int i=0; i<12; i++)
                                    {
                                        if(centipede[i].getVida() == 0)
                                        {
                                            j = i;
                                            break;
                                        }
                                        else
                                        {
                                            j = 20;
                                        }
                                    }

                                    if(j != 20)
                                    {
                                        yRandCabeza = 27 + rand() % 3;

                                        if(yRandCabeza % 2 == 0)
                                        {
                                            centipede[j].setx(1);
                                            centipede[j].setdireccion(0);
                                        }
                                        else
                                        {
                                            centipede[j].setx(31);
                                            centipede[j].setdireccion(1);
                                        }

                                        centipede[j].setcabeza(true);
                                        centipede[j].sety(yRandCabeza);
                                        centipede[j].setVida(1);
                                        centipede[j].setRegreso(false);
                                    }
                                }
                            }
                        }

                        pulguita.printpulga(2,  n, hongos, colision);

                        arana.PrintArania(1, n, hongos, colision);

                        scorpion.printescorpion(1, n, hongos, colision);

                        if (colision != 0){
                            haydisparo = 0;
                            colision = 0;
                        }

                        xyjugador[2] = jugador.Movimiento(disparo, hongos, n, xyjugador, pulguita, arana, scorpion, centipede, haydisparo);


                        if (CentipedesMuertos() > contadorcentipedes){

                            puntaje=puntaje+((CentipedesMuertos() - contadorcentipedes)*50);
                            contadorcentipedes=CentipedesMuertos();
                        }

                        if (pulguita.getvida()==0){
                            puntaje=puntaje+pulguita.getpuntos();
                            pulguita.setvida(-1);
                        }

                        if (scorpion.getvida()==0){
                            puntaje=puntaje+scorpion.getpuntos();
                            scorpion.setvida(-1);
                        }

                        if (arana.getvida()==0){
                            arana.setpuntos(disparo.getdist());
                            puntaje=puntaje+arana.getpuntos();
                            arana.setvida(-1);
                        }

                        colision = disparo.Disparar(xyjugador[0], xyjugador[1], hongos, n, pulguita, arana, scorpion, centipede, colision);


                        if (ValidarVidas(xyjugador) == 1)
                        {
                            pulguita.Restart(pulgastart, pulgax);
                            arana.Restart(aranastart, aranax);
                            scorpion.Restart(escorpionstart, escorpionx);
                            for (i = 0; i < 12; i++)
                            {
                                centipede[i].Restart();
                            }
                            xyjugador[0] = 0;
                            xyjugador[1] = 0;
                            jugador.Restart();
                            if (puntaje>mejorpuntaje){mejorpuntaje=puntaje;}
                            tablero(jugador.getvidas(), puntaje, mejorpuntaje, nivel);
                            break;
                        }
                        if (CentipedesMuertos() == 12)
                        {
                            pulguita.Restart(pulgastart, pulgax);
                            arana.Restart(aranastart, aranax);
                            scorpion.Restart(escorpionstart, escorpionx);
                            xyjugador[0] = 0;
                            xyjugador[1] = 0;
                            jugador.Restart();
                            if (puntaje>mejorpuntaje){mejorpuntaje=puntaje;}
                            tablero(jugador.getvidas(), puntaje, mejorpuntaje, nivel);
                            break;
                        }

                        if (xyjugador[0] != 0 and colision == 0){
                            haydisparo = 1;
                            }

                        xyjugador[0] = 0;
                        xyjugador[1] = 0;
                        Sleep(velocidad);
                        puntos(puntaje);
                        cooldown--;
                        n++;
                    }

                }
                if (nivel == 4)
                {
                    victoria(puntaje);
                }


                else
                {
                    if (jugador.getvidas() == 0)
                    {
                        muerte(puntaje);
                    }
                }
                on = 1;
                break;

            case 2:
                sounds.snd_select(1);
                creditos_instrucciones(opc_menu);
                break;

            case 3:
                sounds.snd_select(1);
                creditos_instrucciones(opc_menu);
                break;

            case 4:
                sounds.snd_select(2);
                on=0;
                gotoxy(0, 33);
                return 0;
                break;
        }

    }
    gotoxy(0, 33);
    return 0;
}

