#include "Jugador.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Disparo.h"
#include "Hongo.h"
#include "Pulga.h"
#include "Arania.h"
#include "Escorpion.h"
#include "Sonidos.h"

#define Arriba 72
#define Abajo 80
#define Izquierda 75
#define Derecha 77
#define Espacio 32

using namespace std;



Jugador::Jugador(int x, int y, int vidas)
{
    this->x = 16;
    this->y = 32;
    this->vidas = vidas;
}

Jugador::Jugador()
{

}

void Jugador::color(int X)
{
SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),X);
}

void Jugador::gotoxy(int x, int y)
{
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon, dwPos);
}

void Jugador::ValidacionContacto(int x, int y, Pulga pulguita, Arania arana, Escorpion scorpion, Centipede centipede[12])
{
    Sonidos player;
    int m = 0;
    int xarana = 0;
    int cara = 0;
    if (x == pulguita.getx() and y == pulguita.gety())
    {
        this->vidas--;
        player.snd_death(2);
        while(m<4)
        {
            color(4);
            gotoxy(x,y);
            printf("%c", 190);
            Sleep(100);
            color(6);
            gotoxy(x,y);
            printf("%c",42);
            Sleep(100);
            m++;
        }
    }
    if (x == arana.getx() and y == arana.gety()
    or x == arana.getx()+1 and y == arana.gety()
    or x == arana.getx()+2 and y == arana.gety())
    {
        if (x == arana.getx() and y == arana.gety()){
            xarana = x;
        }
        if (x == arana.getx()+1 and y == arana.gety()){
            xarana = x-1;
        }
        if (x == arana.getx()+2 and y == arana.gety()){
            xarana = x-2;
        }
        player.snd_death(2);
        this->vidas--;
        while(m<4)
        {
            color(4);
            gotoxy(x,y);
            printf("%c", 190);
            Sleep(100);
            color(8);
            gotoxy(xarana,y);
            arana.printarana();
            Sleep(100);
            m++;
        }
    }
    if (x == scorpion.getx() and y == scorpion.gety())
    {
        player.snd_death(2);
        this->vidas--;
        while(m<4)
        {
            color(4);
            gotoxy(x,y);
            printf("%c", 190);
            Sleep(100);
            color(3);
            gotoxy(x,y);
            printf("%c",245);
            Sleep(100);
            m++;
        }
    }
    for (int i = 0; i<12; i++)
    {
        if (x == centipede[i].getx() and y == centipede[i].gety())
        {
            player.snd_death(2);
            this->vidas--;
            if (centipede[i].getcabeza() == true)
            {
                cara = 02;
            }
            else
            {
                cara = 207;
            }
            while(m<4)
            {
                color(4);
                gotoxy(x,y);
                printf("%c", 190);
                Sleep(100);
                color(2);
                gotoxy(x,y);
                printf("%c",cara);
                Sleep(100);
                m++;
            }
        }
    }
}

int Jugador::Movimiento(Disparo disparo, Hongo hongos[170], int n, int xyjugador[2], Pulga pulguita, Arania arana, Escorpion scorpion, Centipede centipede[12], int haydisparo)
{
    color(7);
    Sonidos player;
    this->disparo = disparo;
    int vidaAnterior = this->vidas;
    char ultimatecla;
    char tecla;
    int vdisparo = 0;
    gotoxy(x,y);
    printf("%c", 190);
    int hayhongos = 0;
    int xhongo = 0;
    int yhongo = 0;
    int m = 0;
    ValidacionContacto(x, y, pulguita, arana, scorpion, centipede);

    if(kbhit() and vidaAnterior == this->vidas)
    {
        tecla = getch();
        gotoxy(x,y);
        cout << " ";
        if(tecla == Espacio and haydisparo == 0)
        {
            player.snd_disparo(1);
            gotoxy(20,30);
            xyjugador[0] = x;
            xyjugador[1] = y;
        }
        if(tecla == Izquierda)
        {
            x--;
        }
        if(tecla == Derecha)
        {
            x++;
        }
        if(tecla == Arriba)
        {
            y--;
        }
        if(tecla == Abajo)
        {
            y++;
        }
        ultimatecla = tecla;
        //Validacion cuando se topa con hongos
        for (int i = 0; i<170; i++)
        {
            if (x == hongos[i].getx() and y == hongos[i].gety())
            {
                xhongo = hongos[i].getx();
                yhongo = hongos[i].gety();
            }

            if(hongos[i].getestado() == 5) break;
        }
        if (x == xhongo and tecla == Izquierda)
        {
            x++;
        }
        if (x == xhongo and tecla == Derecha)
        {
            x--;
        }
        if (y == yhongo and tecla == Arriba)
        {
            y++;
        }
        if (y == yhongo and tecla == Abajo)
        {
            y--;
        }
        gotoxy(x,y);
        if (x > 0 and x < 32 and y > 27 and y < 33)
        {
            printf("%c", 190);
        }
        else
        {
            if (x<=0 or x == xhongo)
            {
                x++;
                gotoxy(x,y);
                printf("%c", 190);
            }
            if (x>=32 or x == xhongo)
            {
                x--;
                gotoxy(x,y);
                printf("%c", 190);
            }
            if (y<=27 or y == yhongo)
            {
                y++;
                gotoxy(x,y);
                printf("%c", 190);
            }
            if (y>32 or y == yhongo)
            {
                y--;
                gotoxy(x,y);
                printf("%c", 190);
            }
        }
    }
    if (vidaAnterior != this->vidas)
    {
        xyjugador[0] = -1;
        xyjugador[1] = -1;
    }


    return(xyjugador[2]);
}

int Jugador::getvidas()
{
    return(vidas);
}

void Jugador::setx(int x)
{
    this->x = x;
}

void Jugador::sety(int y)
{
    this->y = y;
}

void Jugador::Restart()
{
    gotoxy(x,y);
    cout<<" ";
    x = 0;
    y = 0;
}


