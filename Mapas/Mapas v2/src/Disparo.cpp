#include "Disparo.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Hongo.h"
#include "Pulga.h"
#include "Escorpion.h"
#include "Sonidos.h"

#define Arriba 72
#define Abajo 80
#define Izquierda 75
#define Derecha 77
#define Espacio 32

using namespace std;

Disparo::Disparo(Hongo hongos[170])
{
    for (int x = 0; x<170; x++)
    {
        this->hongos[x] = hongos[x];
    }
}

Disparo::Disparo()
{

}

void Disparo::gotoxy(int x, int y)
{
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon, dwPos);
}

int Disparo::PosicionHongos(int x, int y, Hongo hongos[170])
{
    int estado;
    int colision = 0;
    for (int i = 0; i<170; i++)
    {
        if (x == hongos[i].getx() and y == hongos[i].gety() and colision == 0)
        {
            colision = 1;
            estado = hongos[i].getestado();
            estado--;
            hongos[i].setestado(estado);
            if (estado > 0)
            {
                color(4);
                gotoxy(x,y+1);
                printf("%c", 179);
                Sleep(10);
                hongos[i].printhongo();
            }
            else
            {
                color(4);
                gotoxy(x,y+1);
                printf("%c", 179);
                Sleep(10);
                hongos[i].printhongo();
                hongos[i].setx(0);
                hongos[i].sety(0);
            }
        }
    }
    return(colision);
}

void Disparo::color(int X)
{
SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),X);
}

int Disparo::PosicionPulga(int x, int y, Pulga pulguita)
{
    int colision = 0;
    if (x == pulguita.getx() and y == pulguita.gety() and colision == 0)
    {
        colision = 2;
        gotoxy(x,y+1);
        printf("%c", 179);
        Sleep(100);
        gotoxy(x,y+1);
        cout<<" ";
        gotoxy(x,y);
        printf("%c", 248);
        Sleep(100);
        gotoxy(x,y);
        cout<<" ";
    }
    return(colision);
}

int Disparo::PosicionArania(int x, int y, Arania arana)
{
    int estado;
    int colision = 0;
    int vida = 1;
    if (y == arana.gety() and x == arana.getx() and colision == 0)   //le das en la izquierda
    {
        arana.SetLastY(y);
        colision = 3;
        gotoxy(x,y+1);
        printf("%c", 179);
        Sleep(100);
        gotoxy(x,y+1);
        cout<<" ";
        gotoxy(x,y);
        printf("%c", 248);
        if (x<=30){
            printf("%c", 248);
        }
        if (x<=29){
            printf("%c", 248);
        }
        Sleep(100);
        gotoxy(x,y);
        cout<<" ";
        if (x<=30){
            cout<<" ";
        }
        if (x<=29){
            cout<<" ";
        }
    }
    if (y == arana.gety() and x-1 == arana.getx() and colision == 0) //le das en el centro
    {
        arana.SetLastY(y);
        colision = 3;
        gotoxy(x,y+1);
        printf("%c", 179);
        Sleep(100);
        gotoxy(x,y+1);
        cout<<" ";
        gotoxy(x-1,y);
        printf("%c", 248);
        printf("%c", 248);
        if (x<=29){
            printf("%c", 248);
        }
        Sleep(100);
        gotoxy(x-1,y);
        if (x<=29){
            cout<<"   ";
        }else{
            cout<<"  ";
        }
    }
    if (y == arana.gety() and x-2 == arana.getx() and colision == 0) //le das en la derecha
    {
        arana.SetLastY(y);
        colision = 3;
        Sleep(100);
        gotoxy(x,y+1);
        cout<<" ";
        gotoxy(x-2,y);
        printf("%c", 248);
        printf("%c", 248);
        if (x<=29){
            printf("%c", 248);
        }
        Sleep(100);
        gotoxy(x-2,y);
        cout<<"   ";
    }
    if(colision==3){
        if(arana.GetLastY()>=25){
             setdist(900);
        }
        if(arana.GetLastY()>=15 && arana.GetLastY()<25){
             setdist(600);
        }
        if(arana.GetLastY()<15){
             setdist(300);
        }
    }
    return(colision);
}

int Disparo::PosicionEscorpion(int x, int y, Escorpion scorpion)
{
    int estado;
    int colision = 0;
    int vida = 1;

    if (x == scorpion.getx() and y == scorpion.gety() and colision == 0)
    {
        colision = 4;
        gotoxy(x,y+1);
        printf("%c", 179);
        Sleep(100);
        gotoxy(x,y+1);
        cout<<" ";
        gotoxy(x,y);
        printf("%c", 248);
        Sleep(100);
        gotoxy(x,y);
        cout<<" ";
    }
    return(colision);
}

int Disparo::PosicionCentipede(int x, int y, Centipede centipede[12])
{
    int colision = 0;

    for(int i = 0; i < 12; i++)
    {
        if (x == centipede[i].getx() and y == centipede[i].gety() and colision == 0)
        {
            colision = i;
            gotoxy(x,y+1);
            printf("%c", 179);
            Sleep(100);
            gotoxy(x,y+1);
            cout<<" ";
            gotoxy(x,y);
            printf("%c", 248);
            Sleep(100);
            gotoxy(x,y);
            cout<<" ";
            centipede[i].setVida(0);
        }
    }
    return(colision);
}


int Disparo::Disparar(int lastx, int y, Hongo hongos[170], int n,
                      Pulga pulguita, Arania arana, Escorpion scorpion,
                      Centipede centipede[12], int colision)
{
    Sonidos bala;
    int on = 1;
    if (x != 0 and y != 0)
    {
        if (x <= 31)
        {
            gotoxy(x,i);
            cout<<" ";
        }
        if (i-1 > 1)
        {
            gotoxy(x,i-1);
            cout<<" ";
        }
        limite = n + 32;
        i = y;
        this->y = y;
        x = lastx;
    }
    if (n> limite)
    {
        limite = 0;
        on = 0;
    }
    gotoxy(x,i);
    if (n<=limite and i > 2 and on == 1)
    {
        color(4);
        i --;
        //si esta enfrente del jugador
        if (n <= limite-31)
        {
            gotoxy(x,i);
            printf("%c", 179);
            Sleep(10);
        if (PosicionHongos(x, i, hongos) != 0)
        {
            colision = 1;
            on = 0;
        }
        else
        {
            if (PosicionPulga(x, i, pulguita) != 0)
            {
                colision = 2;
                on = 0;
            }
            else
            {
                if (PosicionArania(x, i, arana) != 0)
                {
                    colision = 3;
                    on = 0;
                }
                else
                {
                    if (PosicionEscorpion(x, i, scorpion)  != 0)
                    {
                        colision = 4;
                        on = 0;
                    }
                    else
                    {
                        if (PosicionCentipede(x, i, centipede) != 0)
                        {
                            colision = 5;
                            on = 0;
                        }
                        else
                        {
                            colision = 0;
                        }
                    }
                }
            }
        }
        }

        //si no hay nada enfrente del jugador
        if (colision == 0 and on == 1)
        {
            if (i-1 > 2 and n > limite-31)
            {
                gotoxy(x,i-1);
                printf("%c", 179);
                gotoxy(x,i+1);
                cout<<" "<<endl;
                gotoxy(x,i);
                cout<<" "<<endl;
            }
            if (PosicionHongos(x, i-1, hongos) != 0)
            {
                gotoxy(x,i);
                cout<<" ";
                colision = 1;
                on = 0;
            }
            else
            {
                if (PosicionPulga(x, i-1, pulguita) != 0)
                {
                    gotoxy(x,i);
                    cout<<" ";
                    colision = 2;
                    on = 0;
                }
                else
                {
                    if (PosicionArania(x, i-1, arana) != 0)
                    {
                        gotoxy(x,i);
                        cout<<" ";
                        colision = 3;
                        on = 0;
                    }
                    else
                    {
                        if (PosicionEscorpion(x, i-1, scorpion)  != 0)
                        {
                            gotoxy(x,i);
                            cout<<" ";
                            colision = 4;
                            on = 0;
                        }
                        else
                        {
                            if (PosicionCentipede(x, i-1, centipede) != 0)
                            {
                                gotoxy(x,i);
                                cout<<" ";
                                colision = 5;
                                on = 0;
                            }
                            else
                            {
                                if (i != 1 and i != y)
                                {
                                    gotoxy(x, i);
                                    cout<<" ";
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (i == 3)
    {
        colision = 6;
    }
    if (on == 0)
    {
        x = 99;
        i = 0;
        limite = 0;
    }
    if (colision!=0 && colision !=1){
        bala.snd_centipedehit(1);
    }
    return(colision);
}

void Disparo::setdist(int d){
    distancia=d;
}

int Disparo::getdist(){
    return distancia;
}
