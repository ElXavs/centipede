#include "Sonidos.h"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"

Sonidos::Sonidos()
{
}

void Sonidos::snd_coordinar(int ara, int cent, int scorp, int pul, int opcion){

    switch(ara){
    case 0:

        switch(cent){
        case 0:

            switch(scorp){
            case 0:

                switch(pul){
                case 1:
                    snd_pulga(opcion);
                break;
                }

            break;

            case 1:

                switch(pul){
                case 0:
                snd_scorpion(opcion);
                break;

                case 1:
                snd_scorppul(opcion);
                break;
                }

            break;
            }

        break;
        case 1:

            switch(scorp){
            case 0:

                switch(pul){
                case 0:
                snd_centipedesteps(opcion);
                break;

                case 1:
                snd_centpul(opcion);
                break;
                }

            break;

            case 1:

                switch(pul){
                case 0:
                snd_scorpcent(opcion);
                break;

                case 1:
                snd_scorpcentpul(opcion);
                break;
                }

            break;
            }
        break;
        }
    break;
    case 1:

        switch(cent){
        case 0:

            switch(scorp){
            case 0:

                switch(pul){
                case 0:
                snd_arania(opcion);
                break;

                case 1:
                snd_arapul(opcion);
                break;
                }

            break;

            case 1:

                switch(pul){
                case 0:
                snd_arascorp(opcion);
                break;

                case 1:
                snd_arascorppul(opcion);
                break;
                }

            break;
            }

        break;
        case 1:

            switch(scorp){
            case 0:

                switch(pul){
                case 0:
                snd_aracent(opcion);
                break;

                case 1:
                snd_aracentpul(opcion);
                break;
                }

            break;

            case 1:

                switch(pul){
                case 0:
                    snd_arascorpcent(opcion);
                break;

                case 1:
                    snd_enemies(opcion);
                break;
                }

            break;
            }
        break;
        }
    break;
    }
}

void Sonidos::snd_arania(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Arania.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Arania.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Arania.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_centipedehit(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Centipede_Hit.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Centipede_Hit.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Centipede_Hit.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_centipedesteps(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Centipede_Steps.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Centipede_Steps.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Centipede_Steps.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_death(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Death.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Death.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Death.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_disparo(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Disparo.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Disparo.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Disparo.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_hongos(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Hongos.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Hongos.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Hongos.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_level(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Next_Level.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Next_Level.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Next_Level.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_pulga(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Pulga.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Pulga.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Pulga.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_scorpion(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Scorpion.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Scorpion.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Scorpion.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_win(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Win.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Win.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Win.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_aracent(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("AraCent.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("AraCent.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("AraCent.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_aracentpul(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("AraCentPul.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("AraCentPul.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("AraCentPul.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_arapul(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("AraPul.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("AraPul.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("AraPul.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_arascorp(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("AraScorp.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("AraScorp.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("AraScorp.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_arascorpcent(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("AraScorpCent.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("AraScorpCent.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("AraScorpCent.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_arascorppul(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("AraScorpPul.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("AraScorpPul.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("AraScorpPul.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_centpul(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("CentPul.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("CentPul.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("CentPul.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_scorpcent(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("ScorpCent.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("ScorpCent.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("ScorpCent.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_scorpcentpul(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("ScorpCentPul.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("ScorpCentPul.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("ScorpCentPul.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_scorppul(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("ScorpPul.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("ScorpPul.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("ScorpPul.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_enemies(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Enemies.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Enemies.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Enemies.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_menu(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Menu.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Menu.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Menu.wav"),NULL,SND_ASYNC|SND_LOOP);}
}

void Sonidos::snd_select(int opcion){

    if (opcion==0){PlaySound(NULL,NULL,0);}
    if (opcion==1){PlaySound(TEXT("Select.wav"),NULL,SND_ASYNC);}
    if (opcion==2){PlaySound(TEXT("Select.wav"),NULL,SND_SYNC);}
    if (opcion==3){PlaySound(TEXT("Select.wav"),NULL,SND_ASYNC|SND_LOOP);}
}
